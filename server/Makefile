#!make

# This makefile sets up WSL distros to share files with each other, by way of bind mounts.

GENERATED = generated.mk
SERVICE = mount-bind-wsl-server.service

SHARE_BASE ?= /mnt/wsl
SHARE_USER ?= 1000

-include generated.mk

all: setup-vars setup-systemd

.PHONY: .check-vars
.check-vars:
	@test -n "${SHARE_BASE}" || { echo "SHARE_BASE is required (ex: /mnt/wsl)"; exit 1; }
	@test -d "${SHARE_BASE}" || { echo "SHARE_BASE ${SHARE_BASE} does not exist"; exit 1; }
	@test -n "${SHARE_SOURCE}" || { echo "SHARE_SOURCE is required (ex: /home/myuser/data)"; exit 1; }
	@test -d "${SHARE_SOURCE}" || { echo "SHARE_SOURCE ${SHARE_SOURCE} does not exist"; exit 1; }
	@test -n "${SHARE_TARGET}" || { echo "SHARE_TARGET is required (ex: myuser-data)"; exit 1; }
	@test -n "${SHARE_USER}" || { echo "SHARE_USER is required (ex: 1000)"; exit 1; }
	@id "${SHARE_USER}" 1>/dev/null 2>/dev/null || { echo "SHARE_USER ${SHARE_USER} is invalid"; exit 1; }

.PHONY: setup-vars
setup-vars: .check-vars
	@echo -n "" > ${GENERATED}
	@echo "DATA_DISTRO=${WSL_DISTRO_NAME}" >> ${GENERATED}
	@echo "SHARE_BASE=${SHARE_BASE}" >> ${GENERATED}
	@echo "SHARE_SOURCE=${SHARE_SOURCE}" >> ${GENERATED}
	@echo "SHARE_TARGET=${SHARE_TARGET}" >> ${GENERATED}
	@echo "SHARE_USER=${SHARE_USER}" >> ${GENERATED}

.PHONY: setup-systemd
setup-systemd: .check-vars
	@cat ${SERVICE} | \
		sed \
			-e "s|%SHARE_BASE%|${SHARE_BASE}|g" \
			-e "s|%SHARE_SOURCE%|${SHARE_SOURCE}|g" \
			-e "s|%SHARE_TARGET%|${SHARE_TARGET}|g" \
			-e "s|%SHARE_USER%|${SHARE_USER}|g" | \
		sudo tee /etc/systemd/system/${SERVICE}
	@echo "# Successfully wrote /etc/systemd/system/${SERVICE}"
	@sudo systemctl daemon-reload
	@sudo systemctl enable ${SERVICE}
	@sudo systemctl start ${SERVICE}
	@sudo systemctl status ${SERVICE} --no-pager -l
