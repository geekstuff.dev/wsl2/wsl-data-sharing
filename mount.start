#!/bin/bash

set -e

baseDir="$(dirname "$0")"
source $baseDir/server/generated.mk

SOURCE=${SHARE_BASE}/${SHARE_TARGET}
DEST=/home/$(getent passwd ${SHARE_USER} | cut -d':' -f1)

if test $(id -u) -ne 0; then
  echo "Must be run as root"
  exit 1
fi

if test "$(hostname)" = "${DATA_DISTRO}"; then
  echo "Cannot run this from the data distro"
  exit 1
fi

if test -e ${DEST}/.mount.paths; then
  MOUNT_PATHS=${DEST}/.mount.paths
elif test -e ${baseDir}/mount.paths; then
  MOUNT_PATHS=${baseDir}/mount.paths
else
  echo "No mount.paths to mount"
fi

handle () {
  local DIR_SOURCE="${SOURCE}/$1"
  local DIR_DEST="${DEST}/$1"

  if ! test -e $DIR_SOURCE; then
    return 0
  fi

  if ! test -e $DIR_DEST; then
    if test -d $DIR_SOURCE; then
      mkdir -p $DIR_DEST
      touch $DIR_DEST/not_mounted
      chown -R $SHARE_USER:$SHARE_USER $DIR_DEST
      echo "$DIR_DEST created"
    else
      touch $DIR_DEST
    fi
  fi

  if ! cat /proc/mounts | grep -q $DIR_DEST; then
    mount --bind $DIR_SOURCE $DIR_DEST
    echo "$DIR_DEST mounted"
  else
    echo "$DIR_DEST already mounted"
  fi
}

echo "Mounting paths from ${MOUNT_PATHS}"
while read -r path; do
  handle $path
done < ${MOUNT_PATHS}
