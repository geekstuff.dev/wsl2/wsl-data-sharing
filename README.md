# WSL - distro-data-sharing-scripts

Collection of WSL scripts to deal with WSL distro acting as a data server,
and others acting as data clients.

The main goal is to be able to work and interate on Linux related projects,
in Ubuntu 22.04, 20.04, Alpine, and even Windows host, while all of them
have access to the same data stored in Linux and closer to the docker engines.

Each of those shares are mounted with `mount --bind`, which means they
will truly act like native local linux folders in each client distro

## High level

- SERVER: A dedicated WSL distro for data, with systemd enabled.
- CLIENTS:
  - a wsl distro with a systemd service that mounts data automatically
  - a wsl distro without systemd and instead on user login does that mount

## Import a distro with your own name

One thing that is leveraged here is the ability to import a WSL distribution
under a name of your choosing.

Manually this implies something like creating an official Ubuntu distro,
setting it up if needed, exporting it, removing it, and re-import it one
or multiple times, under a specific name.

To make that process much faster, we are leveraging an open source project
that [pre-builds importable WSL images](https://gitlab.com/geekstuff.dev/wsl2/docker-dev). Not precisely tailored to the needs here but can be tweaked and eventually a variant might get added to do just that.

## Status

Work in progress

## Flow

Let's take an example use case where a `gs-` prefix is used on a set of WSL distributions.

### Create a WSL distro dedicated for your data

Import and configure [WSL distro with systemd enabled](https://gitlab.com/geekstuff.dev/wsl2/docker-dev/ubuntu-22.04-systemd/-/releases)
as `gs-data`. (with instructions in that link)

The distro link above is the easiest option, but you can absolutely set one on your own for this work.

### Setup server WSL distribution

- Login / start shell into gs-data
- Create new data folder in your linux home folder
- Inside it, clone this project.
- `cd server/` folder
- Run `make` and follow instructions there.
  - example: `SHARE_TARGET=gs-data make` and keep defining the vars it
    tells you to define

Once done, you will see a green status for that server service.

A new git ignored file will also have been generated and should stay there
to help clients also get configured.

### Setup client WSL distributions

#### For a systemd enabled distro

A systemd service gets created and will handle mount / unmount.

- Import and configure a `gs-ubuntu-2204` distro
- Login / start shell into `gs-ubuntu-2204`
- `make -C /mnt/wsl/gs-data/wsl-data-sharing/client install-systemd`

#### For other distros

An addition to user .profile gets added and handles mount on first login.

- Import and configure X distro
- Login / start shell
- `make -C /mnt/wsl/gs-data/wsl-data-sharing/client install-profile`
